/**
 *isBound
 *@author Golga <r-ro@bulko.net>
 *@since 2.2.0 Koregraf
 *@see https://github.com/Bulko/koregraf/blob/koregraf/master/themes/bko-2017/js/tunnel.js
 *@param  String   type
 *@param  Function fn
 *@return Boolean
 */
$.fn.isBound = function(type, fn)
{
	if (
		(
			this.data('eventsisBound') === undefined
			|| this.data('eventsisBound')[type] === undefined
			|| this.data('eventsisBound')[type].length === 0
		)
		&& this.data('eventsisBound') !== type
	)
	{
		this.data("eventsisBound", type);
		return false;
	}
	return true;
};

/**
 *checkVisible
 *@author Golga <r-ro@bulko.net>
 *@since 	https://github.com/Bulko/qant_io_templates
 *@param	String evalType	searched property
 *@return	Bool
 */
$.fn.checkVisible = function( evalType )
{
	evalType = evalType || "visible";

	var vpH = $(window).height(), // Viewport Height
		st = $(window).scrollTop(), // Scroll Top
		y = $(this).offset().top,
		elementHeight = $(this).height();
	if ( evalType === "visible" )
	{
		return ((y < (vpH + st)) && (y > (st - elementHeight)));
	}
	else if ( evalType === "above" )
	{
		return ((y < (vpH + st)));
	}
	return false;
}

/**
 *getYTId
 *@author Golga <r-ro@bulko.net>
 *@since AGG 0.0.1
 *@param  String urlYT
 *@return String
 */
function getYTId( urlYT )
{
	var ytID = urlYT.match( /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/ );
	if ( ytID && ytID[2].length === 11 )
	{
		return ytID[2];
	}
	else
	{
		return 'error';
	}
}

/**
 *trackCF7
 *@author Golga <r-ro@bulko.net>
 *@since Peperiot 1.0.0
 *@return Void
 */
function trackCF7()
{
	// Track contact form 7
	//@see https://github.com/Bulko/bulkCform7Tracker/blob/master/js/gaTrack.js
	if ( typeof __gaTracker == 'function' )
	{
		__gaTracker( function() {
			window.ga = __gaTracker;
		});
	}
	document.addEventListener( 'wpcf7mailsent', function( event )
	{
		ga('send', 'event', 'contact-form', 'submit');
	}, false );
}

/**
 *trackPChange
 *@author Golga <r-ro@bulko.net>
 *@since Peperiot 1.0.0
 *@return Void
 */
function trackPChange()
{
	window.ga = window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
	ga('create', '{your-ga}', 'auto');
	ga("set", "location", window.location.href);
	ga("send", "pageview");
}

/**
 *resize16_9
 *@author Golga <r-ro@bulko.net>
 *@since Peperiot 1.0.0
 *@return Int
 */
$.fn.resize16_9 = function()
{
	return $(this).height( $(this).width() * 0.5628205128205128 );
}

/**
 *bkoMenu
 *@author Golga <r-ro@bulko.net>
 *@since Peperiot 1.0.0
 *@param  Array param [description]
 *@return Void
 */
$.fn.bkoMenu = function( param )
{
	var defaultParam = {
		'mainSelector': 'body',
		'menuSelector': this,
		'closeSelector': ".mobile-fermeture",
		'btnSelectorSub': ".sub-menu-btn",
		'burgerSelector': '.hamburger',
		'oppenSelector': '.openMenu',
		'oppenSubSelector': '.openSubMenu',
		'oppenBurgerSelector': '.is-active'
	};
	var param = $.extend( defaultParam, param );
	var $burger = $( param.burgerSelector );
	var $burgerAvtive = $( param.burgerSelector + param.oppenBurgerSelector );
	var $close = $( param.closeSelector );
	var isOppen = param.oppenSelector.substring( 1 );
	var isOppenSub = param.oppenSubSelector.substring( 1 );
	var isActive = param.oppenBurgerSelector.substring( 1 );
	var btnSelectorSub = param.btnSelectorSub.substring( 1 );

	// function
	var toggleBkoMenu = function()
	{
		// le selecteur 'li.menu-item-has-children ' + param.btnSelectorSub
		// ne doit en aucun cas être stoqué dans une variable (problème de portée du sélécteur $ avec turbolink)
		$( param.mainSelector ).toggleClass( isOppen );
		$burger.toggleClass( isActive );
	}

	if( $burger.isBound( "bkoMenu" ) == false )
	{
		$burger.on( "click", toggleBkoMenu );
	}
	if( $close.isBound( "bkoMenu" ) == false )
	{
		$close.on( "click", toggleBkoMenu );
	}
	// ajouter flèche sous-menu
	$(".menu-mobile li.menu-item-has-children").prepend("<div class='" + btnSelectorSub + "'><i class=\"fa fa-chevron-right\"></i></div>");
	// le selecteur 'li.menu-item-has-children ' + param.btnSelectorSub
	// ne doit en aucun cas être stoqué dans une variable (problème de portée du sélécteur $ avec turbolink)
	$( '.menu li.menu-item-has-children ' + param.btnSelectorSub ).on( "click", function(e) {
		var toggleBtn = $(this).parent();
		if ( toggleBtn.hasClass( isOppenSub ) )
		{
			toggleBtn.removeClass( isOppenSub );
		}
		else
		{
			$(param.btnSelectorSub).removeClass( isOppenSub );
			toggleBtn.addClass( isOppenSub );
		}
	});
	// reset
	$burgerAvtive.removeClass( isActive );
}



/**
 *bkoCookies
 *@author Noumie <a-le@bulko.net>
 *@since veterinaireaussonne 1.0.0
 *@param  Array param [description]
 *@return Void
 */
$.fn.bkoCookies = function( param )
{
	var defaultParam = {
		'bkoTxt': '<p>En poursuivant votre navigation sur ce site, vous acceptez l\'utilisation de Cookies ou autres traceurs.</p><p>Ceux-ci sont utilisés pour améliorer et personnaliser votre navigation sur le site, réaliser des statistiques et des mesures d\'audiences.</p>',
		'bkoBgColor': '#000000',
		'bkoTxtColor': '#ffffff',
		'bkoTxtDnt': '<p>Do not track activé : Nous respectons votre choix et votre droit à la vie privée.</p>'
	};
	var param = $.extend( defaultParam, param );
	var $body = $("body");
	var bkoCookiesTxt = param.bkoTxt;
	var bkoCookiesTxtDnt = param.bkoTxtDnt;
	var bkoCookiesBgColor = param.bkoBgColor;
	var bkoCookiesTxtColor = param.bkoTxtColor;

	// Class qui vont apparaître dans le body pour gérer l'affichage du bandeau
	var valeurCookieOk = "bkoCookies-button-ok";
	var valeurCookieNotOk = "bkoCookies-button-not-ok";

	var pageWidth = $(window).width();


	// Au loading de la page on vérifie si on a déjà le cookie :
	if ( localStorage.getItem('bkoCookies') !== 'bkoCookies-button-ok' && localStorage.getItem('bkoCookies') != 'bkoCookies-button-not-ok' )
	{
		bkoCookiesLaunch();

	// si on a déjà cliqué sur un bouton  -> cliqué Oui
	} else if ( localStorage.getItem('bkoCookies') === 'bkoCookies-button-ok' ) {
		trackPChange();
		trackCF7();

	// si on a déjà cliqué sur un bouton  -> cliqué Non
	} else if ( localStorage.getItem('bkoCookies') === 'bkoCookies-button-not-ok' ) {
	}

	// html
	function bkoCookiesHtml()
	{
		if ( $body.hasClass('dnt') ) {
			var bkoCookiesHtml = "<div class='bkoCookies-wrapper'><div class='bkoCookies-content'>"+bkoCookiesTxtDnt+"</div><div class='bkoCookies-buttons'><button class='bkoCookies-button bkoCookies-denied' href='#' >Ok</button></div></div>";

		} else {
			var bkoCookiesHtml = "<div class='bkoCookies-wrapper'><div class='bkoCookies-content'>"+bkoCookiesTxt+"</div><div class='bkoCookies-buttons'><button class='bkoCookies-button bkoCookies-accept' href='#' >J'accepte</button><button class='bkoCookies-button bkoCookies-denied' href='#' >Je&nbsp;refuse</button></div></div>";	
		}

		$body.prepend(bkoCookiesHtml);
	}

	// css
	function bkoCookiesCss()
	{
		if ( pageWidth <= 600 )
		{
			$(".bkoCookies-wrapper").addClass("bkoCookies-responsive");
		} else {
			$(".bkoCookies-wrapper").removeClass("bkoCookies-responsive");
		}

		$(".bkoCookies-wrapper").css({
			"position": "fixed",
			"display": "none",
			"bottom": "0",
			"left": "0",
			"flex-direction": "row",
			"justify-content": "center",
			"width": "100%",
			"z-index": "9999",
			"padding": "2rem",
			"transition": "opacity 0.2s ease",
			"color": bkoCookiesTxtColor,
			"background-color": bkoCookiesBgColor,
			"box-shadow": "0 0 0.8rem 0.3rem rgba(0,0,0,0.3)"
		});
		$(".bkoCookies-content").css({
			"display": "flex",
			"flex-direction": "column",
			"justify-content": "center",
			"background-color": bkoCookiesBgColor,
			"color": bkoCookiesTxtColor
		});
		$(".bkoCookies-content p").css({
			"line-height": "1.7rem",
			"font-size": "1.5rem",
			"margin-bottom": "0"
		});
		$(".bkoCookies-buttons").css({
			"display": "flex",
			"flex-direction": "column",
			"justify-content": "center",
			"padding-left": "3%"
		});
		$(".bkoCookies-accept").css({
			"display": "block",
			"margin-bottom": "0.8rem",
			"padding": "0.5rem 2.5rem",
			"background-color": bkoCookiesTxtColor,
			"text-transform": "uppercase",
			"border": "0.1rem solid white",
			"box-shadow": "none",
			"font-size": "1.3rem",
			"cursor": "pointer",
			"color": bkoCookiesBgColor
		});
		$(".bkoCookies-denied").css({
			"display": "block",
			"padding": "0.5rem 2.5rem",
			"background-color": "transparent",
			"text-transform": "uppercase",
			"border": "0.1rem solid white",
			"box-shadow": "none",
			"font-size": "1.3rem",
			"cursor": "pointer",
			"color": bkoCookiesTxtColor
		});
		$(".bkoCookies-denied").mouseover(function() {
			$(this).css({
				"background-color": bkoCookiesTxtColor,
				"color": bkoCookiesBgColor
			});
		});
		// SKIN effet hover bouton refuser
		$(".bkoCookies-denied").mouseover(function() {
			$(this).css({
				"background-color": bkoCookiesTxtColor,
				"color": bkoCookiesBgColor
			});
		}).mouseout(function() {
			$(this).css({
				"background-color": bkoCookiesBgColor,
				"color": bkoCookiesTxtColor
			});
		});
		// SKIN responsive
		$(".bkoCookies-responsive").css({
			"flex-direction": "column",
			"color": bkoCookiesBgColor
		});
		$(".bkoCookies-responsive .bkoCookies-buttons").css({
			"padding-left": "0",
			"padding-top": "2rem"
		});
	}

	// action clic sur Oui -> enregistrement cookie
	function bkoCookiesAccept()
	{
		localStorage.setItem("bkoCookies", valeurCookieOk);
		$body.removeClass(valeurCookieNotOk);
		$body.addClass(valeurCookieOk);
		$(".bkoCookies-wrapper").css("display", "none");
	}

	// action clic sur Non -> enregistrement cookie
	function bkoCookiesDenied()
	{
		localStorage.setItem("bkoCookies", valeurCookieNotOk);
		$body.removeClass(valeurCookieOk);
		$body.addClass(valeurCookieNotOk);
		$(".bkoCookies-wrapper").css("display", "none");
	}

	// init cookieBaner!
	function bkoCookiesLaunch()
	{
		bkoCookiesHtml();
		bkoCookiesCss();
		$(".bkoCookies-wrapper").css('display', 'flex');
	}

	// stockage choix utilisateur
	if( localStorage.getItem('bkoCookies') )
	{
		$body.addClass( localStorage.getItem('bkoCookies') );
	}

	// action au clic sur Oui
	$('.bkoCookies-accept').on("click", function(){
		bkoCookiesAccept();

		// verif turbolinks -> idempotence
		if ( !$body.attr('data-appended') )
		{
			$body.attr('data-appended', 'true');
			trackPChange();
			trackCF7();
		}
	});

	// action au clic sur Non
	$('.bkoCookies-denied').on("click", function(){
		bkoCookiesDenied();
	});
}

