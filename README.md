# bulkWPTheme

## Install

1. Search & replace `{my-theme-name}` by your theme name.
2. Search & replace `{your-ga}` by your GA Tag.
3. Paste bulkWPTheme folder into `wp-content`.
4. Rename it by "your theme name".

## Build asset
Use [gulp-oyana](https://github.com/Oyana/gulp-oyana).

